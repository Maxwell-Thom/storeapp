package store

class StorefrontController {

	def showInventory(){
		println params
		def products = Product.list()
		def nameAndPrice =products.collect{
			[name:it.name, 
			 price:it.price]
		}

		render view:"products", model:[products:nameAndPrice]
	}

    def index() { }
}
