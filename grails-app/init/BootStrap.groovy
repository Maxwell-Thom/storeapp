import store.*

class BootStrap {

    def init = { servletContext ->
    	def p1 = new Product()
    	p1.name = "apples"
    	p1.price = 5.0
    	p1.save()

    	def p2 = new Product()
    	p2.name = "oranges"
    	p2.price = 6.0
    	p2.save()

    }

    def destroy = {
    }
}
